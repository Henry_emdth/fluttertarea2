import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    //Guia de estilos
    //
    //CupertinoApp
    //WidgetsApp
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
          primarySwatch: Colors.green,
          visualDensity: VisualDensity.adaptivePlatformDensity),
      home: MyHomePage(),
      //MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  bool encendido;
  @override
  void initState() {
    super.initState();
    encendido = true;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Material App Bar'),
      ),
      floatingActionButton: FloatingActionButton(
        backgroundColor: Colors.blue,
        onPressed: () {
          setState(() {
            encendido = !encendido;
          });
        },
        child: Icon(Icons.edit),
      ),
      body: Center(
        child: Container(
          child: Row(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(
                height: 100,
                width: 100,
                color: encendido ? Colors.blue : Colors.red,
                child: FlutterLogo(
                  size: 60.0,
                ),
              ),
              Container(
                height: 100,
                width: 100,
                color: encendido ? Colors.red : Colors.blue,
                child: FlutterLogo(
                  size: 60.0,
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
